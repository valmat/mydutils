module myutils.log;

static import logger = std.experimental.logger;

alias LogLevel = logger.LogLevel;

void init(logger.LogLevel level = logger.LogLevel.all)
{
    // https://dlang.org/phobos/std_experimental_logger_core.html#.globalLogLevel
    //all · critical · error · fatal · info · off · trace · warning
    logger.globalLogLevel(level);
}
void init(string fileName, logger.LogLevel level = logger.LogLevel.all)
{
    init(level);
    if(fileName.length) {
        try {
            logger.sharedLog = new logger.FileLogger(fileName);
        } catch(Exception exc) {
            logger.critical (exc.msg);
        }
    }
}

//private
template defaultLogFunction(logger.LogLevel ll)
{
    void defaultLogFunction(
        int    line           = __LINE__,
        string file           = __FILE__,
        string funcName       = __FUNCTION__,
        string prettyFuncName = __PRETTY_FUNCTION__,
        string moduleName     = __MODULE__, A...
    )(lazy A args)
        if ((args.length > 0 && !is(Unqual!(A[0]) : bool)) || args.length == 0)
    {
        static if (logger.isLoggingActiveAt!ll && ll >= logger.moduleLogLevel!moduleName)
        {
            //stdThreadLocalLog
            logger.sharedLog
                .memLogFunctions!(ll)
                .logImpl!(line, file, funcName, prettyFuncName, moduleName)(" [", ll, "]\t", args);
        }
    }

    void defaultLogFunction(
        int    line           = __LINE__,
        string file           = __FILE__,
        string funcName       = __FUNCTION__,
        string prettyFuncName = __PRETTY_FUNCTION__,
        string moduleName     = __MODULE__, A...
    )(lazy bool condition, lazy A args)
    {
        static if (isLoggingActiveAt!ll && ll >= moduleLogLevel!moduleName)
        {
            //stdThreadLocalLog
            logger.sharedLog
                .memLogFunctions!(ll)
                .logImpl!(line, file, funcName, prettyFuncName, moduleName)(condition, " [", ll, "]\t", args);
        }
    }
}

alias trace    = defaultLogFunction!(logger.LogLevel.trace);
alias info     = defaultLogFunction!(logger.LogLevel.info);
alias warning  = defaultLogFunction!(logger.LogLevel.warning);
alias error    = defaultLogFunction!(logger.LogLevel.error);
alias critical = defaultLogFunction!(logger.LogLevel.critical);
//alias fatal    = defaultLogFunction!(LogLevel.fatal);

//Example of use:
//log.trace    ("log.trace    |", "rrr", 1, 3.14);
//log.info     ("log.info     |", "rrr");
//log.warning  ("log.warning  |", "rrr");
//log.critical ("log.critical |", "rrr");
