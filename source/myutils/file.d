module myutils.file;

import std.stdio     : writeln, write, File;
import std.algorithm : map;
import std.file      : remove;

auto fileLines(string fname)
{
    return File(fname, "r").fileLines();
}
auto fileLines(File file)
{
    return file
        .byLine()
        .map!"a.idup";
}
alias FileLines = typeof(fileLines(""));


// Файл, который удаляется после разружения объекта
struct TmpFile
{
private:
    string _name;
    File _file;

public:
    @disable this(this);

    this(string name)
    {
        _name = name;
        _file = File(name, "w");
    }
    ~this()
    {
        _file.close();
        _name.remove();
    }
    auto file()
    {
        return _file;
    }
    void close()
    {
        _file.close();
    }

    void flush()
    {
        _file.flush();
    }

    void write(Args...)(Args args)
        if(Args.length > 0)
    {
        static foreach(arg; args) {
            _file.write(arg);
        }
    }
    void writeln(Args...)(Args args)
        if(Args.length > 0)
    {
        this.write(args);
        _file.writeln();
    }

    alias _file this;
}
