module myutils.json;

import std.json      : JSONType, JSONValue;
import std.conv      : to;
import std.algorithm : map;
import std.array     : array;
import myutils.array : has;
//import vest.json;

string[] strArray(JSONValue v) pure
{
    return v
        .array
        .map!(x => x.str)
        .array;
}

bool isTrue(ref const(JSONValue) v) pure @safe nothrow @nogc
{
    return JSONType.true_ == v.type;
}
bool isTrue(const(JSONValue) *v) pure @safe nothrow @nogc
{
    return JSONType.true_ == v.type;
}

bool fieldIsTrue(string field)(ref const(JSONValue) obj) pure @safe nothrow @nogc
{
    auto fieldValue = obj.checkNull(field);
    return (fieldValue !is null && fieldValue.isTrue);
}


// Удаляет ключи
JSONValue removeKeys(in JSONValue v, string[] fields)
{
    JSONValue ret;
    foreach(key, ref val; v.object) {
        // Копируем поля
        if(!fields.has(key)) {
            ret[key] = val;
        }
    }
    return ret;
}

// Объеденяет два объекта в один
JSONValue mergeWith(in JSONValue lhs, in JSONValue rhs)
{
    JSONValue ret = lhs;
    // Копируем поля
    foreach(key, ref val; rhs.object) {
        ret[key] = val;
    }
    return ret;
}