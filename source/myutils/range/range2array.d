module myutils.range.range2array;

//
// Конвертирует Range в массив
//

import std.range.primitives : ElementType, isInputRange, isInfinite, hasLength;
import std.traits           : Unqual;

deprecated("Use toArray instead")
auto range2array(T, R)(auto ref R rhs)
{
    return rhs.toArray!T;
}

deprecated("Use toArray instead")
auto range2array(R)(auto ref R rhs)
{
    return rhs.toArray;
}

auto toArray(T, R)(auto ref R rhs)
    if(isInputRange!R && !isInfinite!R)
{
    T[] result;
    static if(hasLength!R) {
        result.reserve(rhs.length);
    }
    foreach(ref item; rhs) {
        result ~= item;
    }
    return result;
}
auto toArray(R)(auto ref R rhs)
    if(isInputRange!R && !isInfinite!R)
{
    alias res_t = Unqual!(ElementType!R);
    return rhs.toArray!res_t;
}
