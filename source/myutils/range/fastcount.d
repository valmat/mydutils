module myutils.range.fastcount;

import std.range : hasLength;


size_t fastCount(Range)(Range range) pure nothrow
{
    static if(hasLength!Range) {
        return range.length;
    } else {
        size_t result = 0;
        foreach(ref el; range) {
            ++result;
        }
        return result;
    }
}


nothrow unittest{
    import std.algorithm : filter;
    import std.range : iota;
    
    assert( 4 == ["hi", "hellow", "world", "hi"].fastCount());
    assert( 5 == [1, 2, 3, 4, 5].fastCount());
    assert( 3 == [1, 2, 3, 4, 5].filter!`a%2`.fastCount());
    assert( 5 == iota(1,6).fastCount());
}