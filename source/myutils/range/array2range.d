module myutils.range.array2range;

//
// Конвертирует static array to range
//

auto makeRange(T, size_t N)(in T[N] arr) pure @nogc nothrow
{
    return StaticArrayRange!(T, N)(arr);
}

private:
struct StaticArrayRange(T, size_t N)
{
private:
    T[N] _arr;
    size_t _index = 0;

public:
    this(in T[N] arr) pure @nogc nothrow
    {
        _arr = arr;
    }

    T front() const pure @nogc nothrow
    {
        return _arr[_index];
    }
    void put(T v) pure @nogc nothrow
    {
        _arr[_index] = v;
    }    

    bool empty() const pure @nogc nothrow
    {
        return _index >= N;
    }

    void popFront() pure @nogc nothrow
    {
        ++_index;
    }

    enum size_t length = N;
}    


nothrow unittest
{
    import std.algorithm.comparison : equal;
    import std.algorithm.iteration : map ,filter;

    int[5] a = [1,2,3,4,5];
    assert( a.makeRange().map!`a`.equal([1,2,3,4,5]) );
    assert( a.makeRange().filter!`a%2`.equal([1,3,5]) );
}