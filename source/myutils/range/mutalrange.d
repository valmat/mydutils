module myutils.range.mutalrange;

import std.algorithm  : map, isSorted;
import std.functional : unaryFun;

struct Pair(T, S = T)
{
    T l;
    S r;
}

//
// XXX Redo with any ranges
//
auto mutalRange(alias noVal = void, alias indexChoice = `a`, alias valueChoice = `a`, LT , RT)(in LT[] lhs, in RT[] rhs) nothrow pure
in {
    assert(lhs.map!indexChoice.isSorted());
    assert(rhs.map!indexChoice.isSorted());
} do {
    alias indexF = unaryFun!indexChoice;
    alias valueF = unaryFun!valueChoice;
    
    static if(is(noVal == void)) {
        enum _noValL = valueF(LT.init);
        enum _noValR = valueF(RT.init);
    } else {
        enum _noValL = unaryFun!noVal(LT.init);
        enum _noValR = unaryFun!noVal(RT.init);
    }

    alias valTypeL = typeof(valueF(LT.init));
    alias valTypeR = typeof(valueF(RT.init));

    //static struct MutalRange(alias indexChoice, alias valueChoice, LT , RT)
    static struct MutalRange(alias indexChoice, alias valueChoice)
    {
    private:
        alias pair_t = Pair!(valTypeL, valTypeR);
        auto _cur = pair_t(_noValL, _noValR);
        size_t _l_index = 0, _r_index = 0;
        const(LT)[] _lhs;
        const(RT)[] _rhs;
        bool _valid = true;

    public:

        this(in LT[] lhs, in RT[] rhs) nothrow pure @nogc
        {
            _lhs = lhs;
            _rhs = rhs;
            popFront();
        }

        bool empty() const nothrow pure @nogc
        {
            return !_valid;
        }

        void popFront() nothrow pure @nogc
        {
            if(_l_index < _lhs.length && _r_index < _rhs.length) {
                
                if(indexChoice(_lhs[_l_index]) == indexChoice(_rhs[_r_index])) {
                    _cur = pair_t(valueChoice(_lhs[_l_index]), valueChoice(_rhs[_r_index]));
                    ++_l_index; ++_r_index;
                } else if(indexChoice(_lhs[_l_index]) < indexChoice(_rhs[_r_index])) {
                    _cur = pair_t(valueChoice(_lhs[_l_index]), _noValR);
                    ++_l_index;
                } else {
                    _cur = pair_t(_noValL, valueChoice(_rhs[_r_index]));
                    ++_r_index;
                }

            } else if(_l_index < _lhs.length && _r_index >= _rhs.length) {
                _cur = pair_t(valueChoice(_lhs[_l_index]), _noValR);
                ++_l_index;
            } else if(_l_index >= _lhs.length && _r_index < _rhs.length) {
                _cur = pair_t(_noValL, valueChoice(_rhs[_r_index]));
                ++_r_index;
            } else {
                _valid = false;
            }
        }

        pair_t front() const nothrow pure @nogc
        {
            return _cur;
        }
    }

    return MutalRange!(indexF, valueF)(lhs, rhs);
}

nothrow unittest
{
    import std.range    : iota;
    import std.array    : array;
    import std.typecons : tuple;

    struct X
    {
        int index;
        int value = 0;
    }
    alias P = Pair!int;
    {
        int[] xs1 = [1, 10];
        int[] xs2 = [5, 10];
        P[] mr = mutalRange(xs1, xs2).array;
        assert([Pair!int(1, 0), Pair!int(0, 5), Pair!int(10, 10)] == mr);
    }
    {
        int[] xs1 = iota(1, 20, 2).array;
        int[] xs2 = iota(1, 20, 3).array;
        P[] mr = mutalRange(xs1, xs2).array;
        assert([P(1, 1),P(3,0),P(0,4),P(5,0),P(7,7),P(9,0),P(0,10),P(11,0),P(13,13),P(15,0),P(0,16),P(17,0),P(19,19)] == mr);
    }
    {
        int[] xs1 = iota(1, 33, 3).array;
        int[] xs2 = iota(7, 33, 7).array;
        P[] mr = mutalRange(xs1, xs2).array;
        assert([P(1,0),P(4,0),P(7,7),P(10,0),P(13,0),P(0,14),P(16,0),P(19,0),P(0,21),P(22,0),P(25,0),P(28,28),P(31,0)] == mr);
    }
    {
        int[] xs = iota(1, 8, 3).array;
        P[] mr = mutalRange(xs, xs).array;
        assert([P(1, 1), P(4, 4), P(7, 7)] == mr);
    }
    {
        int[] xs1 = [1, 1, 5, 7, 7];
        int[] xs2 = [1, 1, 7, 7, 7];
        P[] mr = mutalRange(xs1, xs2).array;
        assert([P(1, 1), P(1, 1), P(5, 0), P(7, 7), P(7, 7), P(0, 7)] == mr);
    }
    {
        int[] xs1 = [1, 1, 5, 7, 7];
        int[] xs2 = [1, 1, 7, 7];
        P[] mr = mutalRange(xs1, xs2).array;
        assert([P(1, 1), P(1, 1), P(5, 0), P(7, 7), P(7, 7)] == mr);
    }
    {
        int[] xs1 = [1, 1, 6, 7];
        int[] xs2 = [1, 1, 5, 7];
        P[] mr = mutalRange(xs1, xs2).array;
        assert([P(1, 1), P(1, 1), P(0, 5), P(6, 0), P(7, 7)] == mr);
    }
    {
        float[] xs1 = [1.1, 1.2, 6.0, 7.1];
        float[] xs2 = [1.1, 1.2, 5.0, 7.2];
        auto mr = mutalRange!"0"(xs1, xs2).array;
        assert([Pair!float(1.1,1.1),Pair!float(1.2,1.2),Pair!float(0,5),Pair!float(6,0),Pair!float(7.1,0),Pair!float(0,7.2)] == mr);
    }
    {
        string xs1 = "abc";
        string xs2 = "cde";
        auto mr = mutalRange!`'_'`(xs1, xs2).array;
        assert([Pair!char('a','_'),Pair!char('b','_'),Pair!char('c','c'),Pair!char('_','d'),Pair!char('_','e')] == mr);
    }
    {
        string xs1 = "abc";
        string xs2 = "cde";
        auto mr = mutalRange!(`'_'`, `a`, `cast(char)(10+a)`)(xs1, xs2).array;
        assert([Pair!char('k','_'),Pair!char('l','_'),Pair!char('m','m'),Pair!char('_','n'),Pair!char('_','o')] == mr);
    }
    {
        string xs1 = "cba";
        string xs2 = "edc";
        auto mr = mutalRange!(`95`, `cast(char)('z'-a)`, `cast(char)('r'+'g'-a)`)(xs1, xs2).array;
        assert([Pair!char('_','t'),Pair!char('_','u'),Pair!char('v','v'),Pair!char('w','_'),Pair!char('x','_')] == mr);
    }
    {
        int[] xs = [];
        P[] mr = mutalRange(xs, xs).array;
        assert(!mr.length);
    }
    {
        X[] xs1 = [X(1,1), X(10,10)];
        X[] xs2 = [X(5,5), X(10,10)];
        P[] mr = mutalRange!(x => -1, `a.index`, `a.value`)(xs1, xs2).array;
        assert([P(1, -1), P(-1, 5), P(10, 10)] == mr);
    }
    {
        X[] xs1 = [];
        X[] xs2 = [X(5,5), X(10,10)];
        P[] mr1 = mutalRange!(`-1`, `a.index`, `a.value`)(xs1, xs2).array;
        P[] mr2 = mutalRange!(`-1`, `a.index`, `a.value`)(xs2, xs1).array;
        assert([P(-1, 5), P(-1, 10)] == mr1);
        assert([P(5, -1), P(10, -1)] == mr2);
    }
    {
        auto xs1 = [X(1,1), X(20,20)];
        auto xs2 = [X(5,5), X(10,10)];
        auto mr1 = mutalRange!(void, `a.index`, `a.value`)(xs1, xs2).array;
        auto mr2 = mutalRange!(void, `a.index^^2`, `a.value^^3`)(xs1, xs2).array;

        assert([P(1, 0), P(0, 5),   P(0, 10),   P(20, 0)]   == mr1);
        assert([P(1, 0), P(0, 125), P(0, 1000), P(8000, 0)] == mr2);
    }
    {
        auto xs1 = [X(5,1), X(10,20)];
        auto xs2 = [X(1,5), X(20,10)];
        auto mr1 = mutalRange!(void, `a.index`, `a.value`)(xs1, xs2).array;
        auto mr2 = mutalRange!(void, `a.value`, `a.index`)(xs1, xs2).array;
        assert([P(0, 5), P(1, 0), P(20, 0), P(0, 10)] == mr1);
        assert([P(5, 0), P(0, 1), P(0, 20), P(10, 0)] == mr2);
    }
    {
        auto xs1 = [X(1,1), X(10,10), X(20,20), X(40,40), X(100,100), X(120,120)];
        auto xs2 = [X(5,5), X(7,7),   X(20,20), X(30,30), X(100,100), X(150,150), X(170,170)];
        auto mr = mutalRange!(void, `a.index`, `a.value`)(xs1, xs2).array;
        assert([P(1, 0), P(0, 5), P(0, 7), P(10, 0), P(20, 20), P(0, 30), P(40, 0), P(100, 100), P(120, 0), P(0, 150), P(0, 170)] == mr);
    }
    {
        alias T = Pair!(int, size_t);
        int[]    xs1 = [1, 1, 5, 7, 7];
        size_t[] xs2 = [1, 1, 7, 7, 9];
        auto mr = mutalRange(xs1, xs2).array;
        assert([T(1, 1), T(1, 1), T(5, 0), T(7, 7), T(7, 7), T(0, 9)] == mr);
    }
    {
        alias T = Pair!(string, char);
        auto xs1 = [tuple(1, "1"), tuple(5, "5"), tuple(7, "7"), tuple(8, "8")];
        auto xs2 = [tuple(1L, '1'), tuple(3L, '3'), tuple(7L, '7'), tuple(9L, '9')];
        auto mr = mutalRange!(void, `a[0]`, `a[1]`)(xs1, xs2).array;
        assert([T("1", '1'), T("", '3'), T("5", 255), T("7", '7'), T("8", 255), T("", '9')] == mr);
    }
}