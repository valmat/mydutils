module myutils.range.indexof;

import std.range : enumerate, ElementType;

size_t indexOf(Range, T)(Range r, auto ref T src) pure nothrow @nogc
    if(is(T == ElementType!Range))
{
    foreach (i, ref el; r.enumerate) {
        if(el == src) {
            return i;
        }
    }
    return size_t.max;
}



nothrow unittest{
    assert( 1 == ["hi", "hellow", "world", "hi"].indexOf("hellow"));
    assert( 0 == ["hi", "hellow", "world", "hi"].indexOf("hi"));
    assert( size_t.max == [5,4,7,11,6].indexOf(57));
    assert( 3 == [5,4,7,11,6].indexOf(11));

    import std.range : iota;
    assert(3 == iota(1,6).indexOf(4));
    assert(size_t.max == iota(1,6).indexOf(11));
}