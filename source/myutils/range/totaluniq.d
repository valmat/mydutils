module myutils.range.totaluniq;

//
// Удалят все дубли. Не требует, что бы диапазон был упорядочен
//

import std.array      : array;
import std.algorithm  : map, filter;
import std.functional : unaryFun, binaryFun;
import std.range.primitives : isInputRange, isInfinite, ElementType, hasLength, front;


auto totalUniq(alias predicate = "a == b", Range)(Range input)  pure nothrow
    if(
        isInputRange!Range &&
        !isInfinite!Range  &&
        is(typeof(binaryFun!predicate(input.front, input.front)) == bool)
    )
{
    alias pred = binaryFun!predicate;
    
    ElementType!Range* [] ptrs;
    static if(hasLength!Range) {
        ptrs.reserve(input.length);
    }

    ptrs = input
        .map!( (ref x) => &x )
        .array;

    foreach(size_t i, ref el; ptrs) {
        if(el is null) continue;
        
        for(size_t j = i+1; j < ptrs.length; ++j) {
            if(ptrs[j] && pred(*el, *ptrs[j])) {
                ptrs[j] = null;
            }
        }
    }

    return ptrs
        .filter!(x => x)
        .map!(x => *x);
}


nothrow unittest
{
    import std.algorithm.comparison : equal;

    auto x = [1,2,1,5,6,1,1,2,3,4,3];

    assert( x.totalUniq.equal([1, 2, 5, 6, 3, 4]) );
    assert( x.totalUniq!("a%2 == b%4").equal([1, 2, 6, 2, 3, 3]) );
    assert( x.totalUniq!("a%b == 0").equal([1, 2, 5, 6, 4]) );
    assert(x.filter!( x => x%2 ).totalUniq.equal([1, 5, 3]) );
    assert(x.filter!( x => x%2 ).totalUniq!"a >= b".equal([1, 5]) );

    string[] y = ["foo", "bar", "bar", "boom", "next", "fix", "bar"];

    assert( y.totalUniq.equal(["foo", "bar", "boom", "next", "fix"]) );
    assert( y.totalUniq!("a[0] == b[0]").equal(["foo", "bar", "next"]) );
    assert( y.totalUniq!("a.length == b.length").equal(["foo", "boom"]) );
}

