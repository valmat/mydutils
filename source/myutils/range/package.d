module myutils.range;

public import myutils.range.range2array;
public import myutils.range.array2range;
public import myutils.range.indexof;
public import myutils.range.totaluniq;
public import myutils.range.mutalrange;
public import myutils.range.fastcount;


// test:
// cd source
// rdmd -unittest -main --force  myutils/range/package