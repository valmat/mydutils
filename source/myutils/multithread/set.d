module myutils.multithread.set;


import std.array       : array;
import std.range       : iota;
import std.algorithm   : sort, filter;
import std.traits      : isArray;
import std.parallelism : parallel, totalCPUs;
import core.sync.mutex : Mutex;
import core.atomic     : atomicOp;


auto mtSet(T)()
{
    return new shared MtSet!T();
}
auto mtSet(T)(shared T[] data)
{
    return new shared MtSet!T(data);
}
auto mtSet(T)(T[] data)
{
    return mtSet(cast(shared T[]) data);
}
auto mtSet(Range)(Range range)
    if(!isArray!Range)
{
    return mtSet(range.array);
}

shared class MtSet(T)
{
private:
    Mutex _mtx;
    shared T[] _set;

    struct Span
    {
        size_t first;
        size_t length;
    }

public:
    this() shared
    {
        _mtx = new shared Mutex();
        _set = [];
    }
    this(shared T[] data) shared
    {
        _mtx = new shared Mutex();
        _set = data;
        _set.sort();
    }

    // max O(log(n)) search complicacy
    bool has(in T el) shared const
    {
        if(_set.length) {
            
            if(el < _set[0] || el > _set[$-1]) {
                return false;
            }

            Span span = Span(0, _set.length);
            while (span.length > 1) {
                span = (_set[span.first + span.length/2] <= el) ?
                    Span(span.first + span.length/2, span.length - span.length/2) :
                    Span(span.first, span.length/2);
            }
            return (span.length && _set[span.first] == el);
        }
        return false;
    }

    // returns success if could add
    bool add(in T el) shared
    {
        bool result = false;

        _mtx.lock();
        if(!has(el)) {
            result = true;
            _set ~= el;
            _set.sort();
        }
        _mtx.unlock();

        return result;
    }

    void add(shared(const(T[])) els) shared
    {
        if(!els.length) {return;}
        _mtx.lock();
        auto to_add = els.filter!(el => !this.has(el)).array;
        _set ~= to_add;
        _set.sort();
        _mtx.unlock();
    }    

    ref shared(const(T[])) values() shared const
    {
        return _set;
    }

    auto length() shared const
    {
        return _set.length;
    }
}

//
// How to use
// 
//    auto set = mtSet(["abc", "def", "ghi"]);
//    set.writeln;
//    set.has("aaa").writeln;
//    set.has("def").writeln;
//    set.has("dee").writeln;
