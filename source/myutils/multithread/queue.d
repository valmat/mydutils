module myutils.multithread.queue;

import std.array       : array;
import std.range       : iota;
import std.traits      : isArray;
import std.parallelism : parallel, totalCPUs;
import core.sync.mutex : Mutex;
import core.atomic     : atomicOp;

shared(MtQueue!T) mtQueue(T)(shared T[] data)
{
    return new shared MtQueue!T(data);
}
auto mtQueue(T)(T[] data)
{
    return mtQueue(cast(shared T[]) data);
}
auto mtQueue(Range)(Range range)
    if(!isArray!Range)
{
    return mtQueue(range.array);
}

auto mtQueue(T)(T arg, size_t cpus_num)
{
    return mtQueue(arg).setCpus(cpus_num);
}

struct MtQueueNode(T)
{
    T value;
    bool   valid = false;
    alias  valid this;
}

shared class MtQueue(T)
{
private:
    Mutex _mtx;
    shared T[] _queue;
    size_t _cur = 0;
    size_t _cpus_num;

public:
    this(shared T[] data) shared
    {
        _mtx      = new shared Mutex();
        _queue    = data;
        _cpus_num = cast(size_t) totalCPUs();
    }

    shared(MtQueue!T) setCpus(size_t cpus_num)
    {
        _cpus_num = cpus_num;
        return this;
    }
    size_t getCpus() const pure
    {
        return _cpus_num;
    }    

    ref shared(const(T[])) values() shared const
    {
        return _queue;
    }

    MtQueueNode!T retrive() shared
    {
        _mtx.lock();

        MtQueueNode!T opt;
        if(_cur < _queue.length) {
            opt = MtQueueNode!T(_queue[_cur], true);
        }
        
        atomicOp!"+="(this._cur, 1);
        
        _mtx.unlock();
        
        return opt;
    }

    int opApply(int delegate(size_t, ref T) cb)
    {
        int foreach_res = 0;

        immutable(size_t[]) cores = iota(size_t(0), _cpus_num).array;
        
        foreach(core_index; parallel(cores) ) {
            MtQueueNode!T node = retrive();
            while (node.valid) {
                foreach_res = cb(core_index, node.value);
                if(foreach_res) return foreach_res;

                node = retrive();
            }
        }
        return foreach_res;
    } 

    int opApply(int delegate(ref T) cb)
    {
        return opApply((size_t core_index, ref T el) => cb(el));
    } 
}

//
// How to use
// 
//    auto queue = mtQueue(iota(1, 160));
//    queue.values.writeln;
//
//    foreach(i, el; queue) {
//        tuple(i, el).writeln;
//    }
//
//    foreach(el; mtQueue(iota(1, 16)) ) {
//        el.writeln;
//    }
//
//
//    foreach(el; mtQueue(iota(1, 16)).setCpus(8) ) {
//        el.writeln;
//    }
//
//    foreach(el; mtQueue(iota(1, 16), 8) ) {
//        el.writeln;
//    }
//
//    foreach(i, el; mtQueue(["abc", "def", "ghi"])) {
//        tuple(i, el).writeln;
//    }
