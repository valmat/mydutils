module myutils.utils;

import std.stdio     : writeln, stderr;
import std.process   : executeShell;
import std.string    : strip, join;
import std.typecons  : tuple, Tuple;
import std.algorithm : map;
import std.process   : thisProcessID, thisThreadID;
import std.traits    : isIntegral, isSomeChar, isBoolean;
import std.meta      : allSatisfy;

string declination(size_t num, string decl1, string decl2, string decl5)
{
    ushort[] cases = [2, 0, 1, 1, 1, 2];
    string[] decls = [decl1, decl2, decl5];
    ushort index   = (num % 100 > 4 && num % 100 < 20) ? 
        2 :
        cases[(num % 10 < 5) ? num % 10 : 5];
    return decls[index];
}

// For using with map. Expands tuple to arguments of calable
template expandTuple(alias F)
{
    auto expandTuple(Args...)(Tuple!Args args)
    {
        return F(args[0..$]);
    }
}

template expandTupleRange(alias F)
{
    auto expandTupleRange(Range)(Range range)
    {
        return range.map!(expandTuple!F);
    }
}

ubyte codeBools() @nogc pure nothrow
{
    return 0;
}
auto codeBools(Args...)(Args args) @nogc pure nothrow
    if(allSatisfy!(isBoolean, Args))
{
    static if(Args.length <= 8) {
        alias ret_t = ubyte;
    } else static if(Args.length <= 16) {
        alias ret_t = ushort;
    } else static if(Args.length <= 32) {
        alias ret_t = uint;
    } else {
        alias ret_t = ulong;
    }
    ret_t res = 0;
    static foreach(size_t i; 0 .. Args.length) {
        res |= args[i] ? (ret_t(1) << (Args.length - i - 1)) : 0;
    }

    return res;
}

const(char)[T.sizeof * 8] binstr(T)(T val) @nogc pure nothrow
    if(isIntegral!T || isSomeChar!T)
{
    char[T.sizeof * 8] res;
    enum size_t one = 1;
    foreach(size_t i; 0 .. T.sizeof * 8) {
        res[T.sizeof * 8 - i - 1] = (val & (one << i)) ? '1' : '0';
    }
    return res;
}

const(char)[11] fastHash(size_t val) @nogc pure nothrow
{
    char[11] res;
    enum size_t one = size_t.max >> 58;
    foreach(size_t i; 0 .. 11) {
        size_t shift = i * 6;
        size_t index = ((one << shift) & val) >> shift;
        res[i] = index._i2c();
    }
    return res;
}

private char _i2c(T)(T c) nothrow pure @nogc
    if(isIntegral!T || isSomeChar!T)
{
    switch(c) {
        case 0: return '-';

        case 1: .. case 10:
            return cast(char)(c + 48 - 1);

        case 11: .. case 36:
            return cast(char)(c - 10 + 65 - 1);

        case 37: .. case 62:
            return cast(char)(c - 36 + 97 - 1);

        case 63: return '~';
        default: return 0;
    }
}

const(char)[11] threadHash() nothrow
{
    size_t procID  = thisProcessID();
    size_t thrdID  = thisThreadID();
    size_t thrHash = size_t.max ^ ((procID << 46) | thrdID);
    return fastHash( size_t.max ^ ((procID << 46) | thrdID) );
}


nothrow unittest {
    import std.algorithm : fill;
    import std.typecons  : Tuple, tuple;
    import core.stdc.stdint;
    {
        assert(0b101 == codeBools(true,false,true));
        assert(0b1010101 == codeBools(true,false,true,false,true,false,true));
        assert(0b11111111 == codeBools(true,true,true,true,true,true,true,true));
        assert(0 == codeBools(false,false,false));
        assert(0 == codeBools());
        assert(0b1010101010101 == codeBools(true,false,true,false,true,false,true,false,true,false,true,false,true));
        assert(0b1010101010101010101 == codeBools(true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true));
        assert(0b10101010101010101010101010101010101010101010101 == codeBools(true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true));
    }{
        assert("00000101" == ubyte(0b101).binstr());
        assert("01010101" == ubyte(0b1010101).binstr());
        assert("11111111" == ubyte(0b11111111).binstr());
        assert("00000000" == ubyte(0).binstr());
        assert("00000000000000000000000000000000" == 0.binstr());
        assert("00000000000000000001010101010101" == 0b1010101010101.binstr());
        assert("00000000000001010101010101010101" == 0b1010101010101010101.binstr());
        assert("0000000000000000010101010101010101010101010101010101010101010101" == 0b10101010101010101010101010101010101010101010101.binstr());
    }{
        assert("-----------" == fastHash(0));
        assert("4----------" == fastHash(5));
        assert("KKK0-------" == fastHash(0b00000000000001010101010101010101));
        assert("KKKgff1----" == fastHash(0b10101010101010101011010101010101010101));
        assert("UUSwvk1----" == fastHash(0b10101111111010111011011101011111011111));
        assert("~~~~~~~~~~E" == fastHash(size_t.max));
        assert("~~~~~2-----" == fastHash(uint.max));
        assert("~~E--------" == fastHash(ushort.max));
        assert("~~~~~~~~~~6" == fastHash(size_t.max / 2));
        assert("~~~~~0-----" == fastHash(uint.max / 2));
        assert("~~6--------" == fastHash(ushort.max / 2));
        assert("aiicfSwrDfC" == fastHash(0b1101101010001110110110111011011101101010100111101101101101100101));
        assert("KJacJKgiOkA" == fastHash(0b1011101111011001101101101011010101010100100111100101010100010101));
    }{
        size_t v = 1;
        char[64] numstr;
        foreach(size_t i; 0 .. 64) {
            numstr.fill('0');
            numstr[$-1-i] = '1';
            assert(numstr == v.binstr());
            v <<= 1;
        }
    }{
        assert(0b0000000000000000000000000000000000000000000000000000000000000001.fastHash() == "0----------");
        assert(0b0000000000000000000000000000000000000000000000000000000000000010.fastHash() == "1----------");
        assert(0b0000000000000000000000000000000000000000000000000000000000000100.fastHash() == "3----------");
        assert(0b0000000000000000000000000000000000000000000000000000000000001000.fastHash() == "7----------");
        assert(0b0000000000000000000000000000000000000000000000000000000000010000.fastHash() == "F----------");
        assert(0b0000000000000000000000000000000000000000000000000000000000100000.fastHash() == "V----------");
        assert(0b0000000000000000000000000000000000000000000000000000000001000000.fastHash() == "-0---------");
        assert(0b0000000000000000000000000000000000000000000000000000000010000000.fastHash() == "-1---------");
        assert(0b0000000000000000000000000000000000000000000000000000000100000000.fastHash() == "-3---------");
        assert(0b0000000000000000000000000000000000000000000000000000001000000000.fastHash() == "-7---------");
        assert(0b0000000000000000000000000000000000000000000000000000010000000000.fastHash() == "-F---------");
        assert(0b0000000000000000000000000000000000000000000000000000100000000000.fastHash() == "-V---------");
        assert(0b0000000000000000000000000000000000000000000000000001000000000000.fastHash() == "--0--------");
        assert(0b0000000000000000000000000000000000000000000000000010000000000000.fastHash() == "--1--------");
        assert(0b0000000000000000000000000000000000000000000000000100000000000000.fastHash() == "--3--------");
        assert(0b0000000000000000000000000000000000000000000000001000000000000000.fastHash() == "--7--------");
        assert(0b0000000000000000000000000000000000000000000000010000000000000000.fastHash() == "--F--------");
        assert(0b0000000000000000000000000000000000000000000000100000000000000000.fastHash() == "--V--------");
        assert(0b0000000000000000000000000000000000000000000001000000000000000000.fastHash() == "---0-------");
        assert(0b0000000000000000000000000000000000000000000010000000000000000000.fastHash() == "---1-------");
        assert(0b0000000000000000000000000000000000000000000100000000000000000000.fastHash() == "---3-------");
        assert(0b0000000000000000000000000000000000000000001000000000000000000000.fastHash() == "---7-------");
        assert(0b0000000000000000000000000000000000000000010000000000000000000000.fastHash() == "---F-------");
        assert(0b0000000000000000000000000000000000000000100000000000000000000000.fastHash() == "---V-------");
        assert(0b0000000000000000000000000000000000000001000000000000000000000000.fastHash() == "----0------");
        assert(0b0000000000000000000000000000000000000010000000000000000000000000.fastHash() == "----1------");
        assert(0b0000000000000000000000000000000000000100000000000000000000000000.fastHash() == "----3------");
        assert(0b0000000000000000000000000000000000001000000000000000000000000000.fastHash() == "----7------");
        assert(0b0000000000000000000000000000000000010000000000000000000000000000.fastHash() == "----F------");
        assert(0b0000000000000000000000000000000000100000000000000000000000000000.fastHash() == "----V------");
        assert(0b0000000000000000000000000000000001000000000000000000000000000000.fastHash() == "-----0-----");
        assert(0b0000000000000000000000000000000010000000000000000000000000000000.fastHash() == "-----1-----");
        assert(0b0000000000000000000000000000000100000000000000000000000000000000.fastHash() == "-----3-----");
        assert(0b0000000000000000000000000000001000000000000000000000000000000000.fastHash() == "-----7-----");
        assert(0b0000000000000000000000000000010000000000000000000000000000000000.fastHash() == "-----F-----");
        assert(0b0000000000000000000000000000100000000000000000000000000000000000.fastHash() == "-----V-----");
        assert(0b0000000000000000000000000001000000000000000000000000000000000000.fastHash() == "------0----");
        assert(0b0000000000000000000000000010000000000000000000000000000000000000.fastHash() == "------1----");
        assert(0b0000000000000000000000000100000000000000000000000000000000000000.fastHash() == "------3----");
        assert(0b0000000000000000000000001000000000000000000000000000000000000000.fastHash() == "------7----");
        assert(0b0000000000000000000000010000000000000000000000000000000000000000.fastHash() == "------F----");
        assert(0b0000000000000000000000100000000000000000000000000000000000000000.fastHash() == "------V----");
        assert(0b0000000000000000000001000000000000000000000000000000000000000000.fastHash() == "-------0---");
        assert(0b0000000000000000000010000000000000000000000000000000000000000000.fastHash() == "-------1---");
        assert(0b0000000000000000000100000000000000000000000000000000000000000000.fastHash() == "-------3---");
        assert(0b0000000000000000001000000000000000000000000000000000000000000000.fastHash() == "-------7---");
        assert(0b0000000000000000010000000000000000000000000000000000000000000000.fastHash() == "-------F---");
        assert(0b0000000000000000100000000000000000000000000000000000000000000000.fastHash() == "-------V---");
        assert(0b0000000000000001000000000000000000000000000000000000000000000000.fastHash() == "--------0--");
        assert(0b0000000000000010000000000000000000000000000000000000000000000000.fastHash() == "--------1--");
        assert(0b0000000000000100000000000000000000000000000000000000000000000000.fastHash() == "--------3--");
        assert(0b0000000000001000000000000000000000000000000000000000000000000000.fastHash() == "--------7--");
        assert(0b0000000000010000000000000000000000000000000000000000000000000000.fastHash() == "--------F--");
        assert(0b0000000000100000000000000000000000000000000000000000000000000000.fastHash() == "--------V--");
        assert(0b0000000001000000000000000000000000000000000000000000000000000000.fastHash() == "---------0-");
        assert(0b0000000010000000000000000000000000000000000000000000000000000000.fastHash() == "---------1-");
        assert(0b0000000100000000000000000000000000000000000000000000000000000000.fastHash() == "---------3-");
        assert(0b0000001000000000000000000000000000000000000000000000000000000000.fastHash() == "---------7-");
        assert(0b0000010000000000000000000000000000000000000000000000000000000000.fastHash() == "---------F-");
        assert(0b0000100000000000000000000000000000000000000000000000000000000000.fastHash() == "---------V-");
        assert(0b0001000000000000000000000000000000000000000000000000000000000000.fastHash() == "----------0");
        assert(0b0010000000000000000000000000000000000000000000000000000000000000.fastHash() == "----------1");
        assert(0b0100000000000000000000000000000000000000000000000000000000000000.fastHash() == "----------3");
        assert(0b1000000000000000000000000000000000000000000000000000000000000000.fastHash() == "----------7");
    }
}