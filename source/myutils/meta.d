module myutils.meta;

import std.traits    : isType, isTypeTuple, isFunction, isArray, isAssociativeArray, isIterable;
import std.traits    : isNumeric, isPointer, isSomeChar, isSomeString, isBoolean;
import std.conv      : to;
import std.typecons  : isTuple;
import std.meta      : Alias, AliasSeq, staticMap;
import std.range     : ElementType;

import myutils.array : indexOf, has;

// Is field property of T
template isProperty(T, string field)
{
    alias fieldValues = AliasSeq!(__traits(getMember, T, field));
    static if(1 == fieldValues.length) {
        alias fieldValue = Alias!(__traits(getMember, T, field));
        enum  isProperty = !isTypeTuple!(fieldValue) &&  !isType!(fieldValue) && !isFunction!(fieldValue) && !is(typeof(fieldValue) == void);
    } else {
        enum  isProperty = false;
    }
}


// Check if field is accessible
enum isAccessible(T, string field) = __traits(compiles, __traits(getMember, T, field));

enum isPublic(T, string field) = ("public" == __traits(getProtection, mixin("T." ~ field)) );

// Helper function to retrive all structure properties
private
template _retriveProperties(T, string[] fields)
{
    static if(!fields.length) {
        enum string[] _retriveProperties = [];
    } else {
        static if( isAccessible!(T, fields[0]) && isProperty!(T, fields[0])  ) {
            enum _retriveProperties = [fields[0]] ~ _retriveProperties!(T, fields[1..$]);
        } else {
            enum _retriveProperties = _retriveProperties!(T, fields[1..$]);
        }
    }
}

// All structure properties (not members)
enum allProperties(T) = _retriveProperties!(T, [__traits(allMembers, T)]);

// Recive property by index
alias getProperty(T, size_t index) = mixin("T." ~ allProperties!T[index]);


// Compile time map arrays
// maps array SRC with functor F
template ctMap(alias SRC, alias F)
{
    static assert(SRC.length);
    alias result_t = typeof(F!(SRC[0]));
    static if(1 == SRC.length) {
        enum result_t[] ctMap = [F!(SRC[0])];
    } else {
        enum result_t[] ctMap = [F!(SRC[0])] ~ ctMap!(SRC[1..$], F);
        //pragma(msg, "ctMap : ", ctMap);
    }
}

enum bool isExtNumeric(T) = isNumeric!T || isSomeChar!T || isBoolean  !T;

template sizeOf(T)
{
    static if(isExtNumeric!T) {
        enum size_t sizeOf = T.sizeof;
    } else {
        enum size_t sizeOf = _sizeOf!(T, allProperties!T);
    }
    
    private
    template _sizeOf(T, string[] fields)
    {
        static if(0 == fields.length) {
            enum size_t _sizeOf = 0;
        } else static if(1 == fields.length) {
            mixin("enum size_t _sizeOf = typeof(T." ~ fields[0] ~ ").sizeof;");
        } else {
            enum size_t _sizeOf = _sizeOf!(T, [fields[0]]) + _sizeOf!(T, fields[1..$]);
        }
    }
}

template ctMap(Args ...)
{
    enum larr = [ AliasSeq!( staticMap!(_lproj, Args[0 .. $]) ) ];
    enum rarr = [ AliasSeq!( staticMap!(_rproj, Args[0 .. $]) ) ];

    alias lhs_t = typeof(Args[0][0]);
    alias rhs_t = typeof(Args[0][1]);

    enum lget(lhs_t key) = rarr[larr.indexOf(key)];
    enum rget(rhs_t key) = larr[rarr.indexOf(key)];

private:
    template _lproj(alias v) {enum _lproj = v[0];}
    template _rproj(alias v) {enum _rproj = v[1];}
}