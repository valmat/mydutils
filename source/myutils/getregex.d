module myutils.getregex;

// Создаёт регулярку в зависимости от типа сборки.
// Для отладочной сборки создаёт регулярки в рантайме.
// Для релизной в компайлтайме

debug {
    // Run-time regex
    import std.regex : regex;
    //pragma(msg, "Run-time regex");
    auto getRegex(string pattern, string flags = "")()
    {
        //pragma(msg, flags);
        return regex(pattern, flags);
        //return regex(pattern);
    }

} else {
    // Compile-time regex
    import std.regex : ctRegex;
    //pragma(msg, "Compile-time regex");
    template getRegex(string pattern, string flags = "")
    {
        //pragma(msg, flags);
        //pragma(msg, pattern);
        enum getRegex = ctRegex!(pattern, flags);
    }
}
