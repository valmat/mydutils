module myutils;

public import myutils.json;
public import myutils.string;
public import myutils.range;
public import myutils.array;
public import myutils.getregex;
public import myutils.meta;
public import myutils.file;
public import myutils.sys;
public import myutils.utils;
public import myutils.multithread;
public import myutils.serialize;

// test:
// cd source
// rdmd -unittest -main --force  myutils/package