module myutils.sys;

import std.stdio   : writeln, stderr;
import std.process : executeShell;
import std.string  : strip, join;
import std.conv    : to;

string exe(string cmd)
{
    //stderr.writeln('[',cmd,']');
    auto rez = executeShell(cmd);
    if (rez.status != 0) {
        //err("status: '" ~ cmd ~ "' is not 0");
        stderr.writeln("status: cmd is not 0");
    }
    //stderr.writeln("output: ", rez.output.strip);
    return rez.output.strip;
}
string exe()
{
    return null;
}

string exe(Args...)(Args args)
    if(Args.length > 0)
{
    string cmd = args[0];
    static foreach(arg; args[1..$]) {
        cmd ~= ` "` ~ arg.to!string ~ `"`;
    }
    return exe(cmd);
}
string exe(string[] args)
{
    return exe(args.join(' '));
}

