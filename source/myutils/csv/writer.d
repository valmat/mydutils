module myutils.csv.writer;

import std.stdio     : stdout;
import std.algorithm : canFind;
import std.range     : replace, repeat, isInputRange;

// Write a row to a CSV file.
// 
// Params:
//   writer: The output stream.
//   row: The row to write (string[] or strings range).
//   delimiter: The character used to separate columns.
//   quote: The character used to quote values that contain special characters.
void writelnCsv(T, R)(ref T writer, R row, char delimiter = ',', char quote = '"')
    if(isInputRange!R)
{
    bool isFirst = true;
    foreach (ref value; row)
    {
        // Add a delimiter before all columns except the first
        if (!isFirst) {
            writer.write(delimiter);
        } else {
            isFirst = false;
        }

        // Escape quotes in the value by doubling them
        string escapedValue = value.canFind(quote) ?
            value.replace(quote, quote.repeat(2)) :
            value;
        
        // If the value contains the delimiter, quote it
        if (escapedValue.canFind(delimiter)) {
            writer.write(quote, escapedValue, quote);
        } else {
            writer.write(escapedValue);
        }
    }
    
    // End the row with a newline
    writer.writeln();
}

void writelnCsv(R)(R row, char delimiter = ',', char quote = '"')
    if(isInputRange!R)
{
    writelnCsv(stdout, row, delimiter, quote);
}


unittest{

    struct Writer
    {
        char[] buf;
        void write(Args...)(Args args)
        {
            static foreach(arg; args) {
                buf ~= arg;
            }
        }
        void writeln(Args...)(Args args)
        {
            write(args); buf ~= '\n';
        }
    }

    {
        Writer writer;
        
        string[][] data = [
            ["Alice", `"Waiter"`],
            ["Bob", "Security, video control"],
            ["Charlie", "\"The Roof\" electrician"],
            ["Doj", "CTO"],
        ];
        
        foreach (row; data) {
            writelnCsv(writer, row);
        }
        
        assert(writer.buf == 
            "Alice,\"\"Waiter\"\"\n" ~ 
            "Bob,\"Security, video control\"\n" ~ 
            "Charlie,\"\"The Roof\"\" electrician\n" ~ 
            "Doj,CTO\n");
    }{
        Writer writer;

        string[][] data = [
            ["Alice", `'Waiter'`],
            ["Bob, B", "Security, video;; control"],
            ["Charlie", "'The Roof' electrician"],
            ["Doj", "CTO", "CEO,COO", "CFO;CMO"],
            ["", null, ",", ";", ";;", ";;;;","'","''"],
        ];
        
        foreach (row; data) {
            writelnCsv(writer, row, ';', '\'');
        }
        
        assert(writer.buf == 
            "Alice;''Waiter''\n" ~
            "Bob, B;'Security, video;; control'\n" ~
            "Charlie;''The Roof'' electrician\n" ~
            "Doj;CTO;CEO,COO;'CFO;CMO'\n" ~
            ";;,;';';';;';';;;;';'';''''\n");
    }
}