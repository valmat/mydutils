module myutils.string;

//import std.stdio  : writeln;
import std.functional : forward;
import std.string     : indexOf;


enum size_t npos = size_t(-1);

string sliceUntil(T)(string line, auto ref T srch)
{
    size_t pos = line.indexOf(forward!srch);
    return ( npos > pos ) ? line = line[0..pos] : line[0..$];
}
