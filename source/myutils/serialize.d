module myutils.serialize;

import std.traits   : isNumeric;
import myutils.meta : sizeOf, allProperties, isExtNumeric;

ubyte[T.sizeof] to_buf(T)(T num) @nogc pure nothrow
    if (isExtNumeric!T)
{
    ubyte[T.sizeof] buf;
    ubyte* pnum = cast(ubyte*) &num;
    static foreach(size_t i; 0 .. T.sizeof) {
        buf[i] = pnum[i];
    }

    return buf;
}

ubyte[sizeOf!T] to_buf(T)(T obj) @nogc pure nothrow
    if (!isExtNumeric!T)
{
    size_t i_from = 0;
    ubyte[sizeOf!T] buf;

    static foreach(field; allProperties!T) {{
        mixin("auto subbuf = to_buf(obj." ~ field ~ ");");
        static foreach(size_t i; 0 .. subbuf.sizeof) {
            buf[i + i_from] = subbuf[i];
        }
        i_from += subbuf.sizeof;
    }}

    return buf;
}

T from_buf(T)(ubyte[T.sizeof] buf) @nogc pure nothrow
    if (isExtNumeric!T)
{
    T num = *(cast(T*)(buf.ptr));
    return num;
}

T from_buf(T, size_t size)(ubyte[size] buf) @nogc pure nothrow
    if (!isExtNumeric!T)
{
    static assert(sizeOf!T == size);

    auto obj = T();
    size_t i_from = 0;

    static foreach(field; allProperties!T) {{
        mixin("alias type = typeof(obj." ~ field ~ ");");
        ubyte[type.sizeof] subbuf;
        static foreach(size_t i; 0 .. type.sizeof) {
            subbuf[i] = buf[i + i_from];
        }
        mixin("obj." ~ field ~ " = subbuf.from_buf!type;");
        i_from += type.sizeof;
    }}

    return obj;
}

nothrow unittest {
    //import std.stdio;
    import std.typecons : Tuple, tuple;
    import core.stdc.stdint;

    {
        uint a = 14 + 251*256 + 42*256*256 + 5*256*256*256;
        auto buf = to_buf(a);
        //buf.writeln("\t <-buf");
        //a.writeln("\t <-a");
        //from_buf!uint(buf).writeln("\t <-from_buf");
        assert(a == from_buf!uint(buf));
        assert([14, 251, 42, 5] == buf);
    }
    {
        double a = 3.14;
        auto buf = to_buf(a);
        //buf.writeln("\t <-buf");
        //a.writeln("\t <-a");
        //from_buf!double(buf).writeln("\t <-from_buf");
        assert([31, 133, 235, 81, 184, 30, 9, 64] == buf);
        assert(a == from_buf!double(buf));
    }
    {
        float a = 3.14;
        auto buf = to_buf(a);
        //buf.writeln("\t <-buf");
        //a.writeln("\t <-a");
        //from_buf!float(buf).writeln("\t <-from_buf");
        assert([195, 245, 72, 64] == buf);
        assert(a == from_buf!float(buf));
    }
    {
        struct tpl_t1{uint16_t x; float y;}
        tpl_t1 a = tpl_t1(129, 3.14);
        auto buf = to_buf(a);
        //buf.writeln("\t <-buf");
        //a.writeln("\t <-a");
        //from_buf!tpl_t1(buf).writeln("\t <-from_buf");
        assert([129, 0, 195, 245, 72, 64] == buf);
        assert(a == from_buf!tpl_t1(buf));
    }    
    {
        alias tpl_t = Tuple!(ubyte, float);
        tpl_t a = tpl_t(42, 3.14);
        auto buf = to_buf(a);
        //buf.writeln("\t <-buf");
        //a.writeln("\t <-a");
        //from_buf!tpl_t(buf).writeln("\t <-from_buf");
        assert([42, 195, 245, 72, 64] == buf);
        assert(a == from_buf!tpl_t(buf));
    }
    {
        alias tpl_t = Tuple!(double, ubyte, char, size_t);
        tpl_t a = tpl_t(3.14, 42, 'x', size_t.max);
        auto buf = to_buf(a);
        //buf.writeln("\t <-buf");
        //a.writeln("\t <-a");
        //from_buf!tpl_t(buf).writeln("\t <-from_buf");
        assert([31, 133, 235, 81, 184, 30, 9, 64, 42, 120, 255, 255, 255, 255, 255, 255, 255, 255] == buf);
        assert(a == from_buf!tpl_t(buf));
    }
}