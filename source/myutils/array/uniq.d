module myutils.array.uniq;

import myutils.array.indexof;
import std.algorithm : filter;
import std.array     : array;
import std.functional: unaryFun;
import std.range.primitives : front;

//import std.range;
//import std.algorithm;
//import std.stdio;

// Оставляет только уникальные элементы в массиве
nothrow pure
template uniq(alias pred = "a")
    if (is(typeof(unaryFun!pred)))
{
    auto uniq(Range)(Range range)
        //if (isInputRange!(Unqual!Range))
    {
        // накопитель
        alias storage_t = typeof(unaryFun!(pred)(Range.init.front));
        storage_t[] storage;
        
        return range.filter!( (x) {
            auto v = unaryFun!(pred)(x);
            //writeln("!!!!!!!!!!!!! ", v, " $$$$$$$$$$$$$$$$");

            if(storage.indexOf(v) == size_t.max) {
                storage ~= v;
                return true;
            }
            return false;
        });
    }
}


nothrow unittest{
    assert(["1", "2", "1", "6", "1", "2", "4"]
        .uniq.array == ["1", "2", "6", "4"] );

    assert([[0,1], [1,2], [2,1], [3,6], [4,1], [5,2], [6,4]]
        .uniq!"a[1]".array == [[0,1], [1,2], [3,6], [6,4]] );
}