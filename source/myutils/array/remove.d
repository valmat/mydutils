module myutils.array.remove;

import myutils.array.indexof;
import std.algorithm : filter;
import std.array     : array;

// Удаляет элементы массива rhs из массива from
nothrow pure
T[] remove(T)(auto ref T[] from, auto ref T[] rhs)
{
    return from
        .filter!(x => rhs.indexOf(x) == size_t.max)
        .array;
}


nothrow unittest{
    {
        string[] a = ["1", "2", "3", "4", "5", "6"];
        string[] b = ["3", "5"];
        assert(a.remove(b) == ["1", "2", "4", "6"] );
    }{
        int[] a = [1, 2, 3, 4, 5, 6];
        int[] b = [1, 3, 5, 6, 7, 8];
        assert(a.remove(b) == [2, 4] );
    }{
        int[] a = [1, 2, 3, 4, 5, 6];
        assert(a.remove(a) == [] );
    }{
        int[] a = [1, 2, 3, 4, 5, 6];
        assert(a.remove((int[]).init) == [1, 2, 3, 4, 5, 6] );
    }{
        size_t[][] a = [[1, 2], [3, 4], [5, 6]];
        size_t[][] b = [[3, 5],[3, 4]];
        assert(a.remove(b) == [[1, 2], [5, 6]] );
    }
}