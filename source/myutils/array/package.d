module myutils.array;

public import myutils.array.indexof;
public import myutils.array.inarray;
public import myutils.array.combinations;
public import myutils.array.remove;
public import myutils.array.uniq;
public import myutils.array.array2d;
public import myutils.array.fillwith : fillWith;

// test:
// cd source
// rdmd -unittest -main --force  myutils/array/package