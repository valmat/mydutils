module myutils.array.indexof;

//size_t indexOf(T)(auto ref const T[] r, auto ref const T src, size_t start = 0) pure nothrow @nogc
size_t indexOf(T)(in T[] r, in T src, size_t start = 0) pure nothrow @nogc
{
    foreach(size_t i; start .. r.length) {
        if(r[i] == src) {
            return i;
        }
    }
    return size_t.max;
}

size_t indexOf(alias pred, T)(auto ref const T[] r, size_t start = 0)
{
    foreach(size_t i; start .. r.length) {
        if(pred(r[i])) {
            return i;
        }
    }
    return size_t.max;
}

nothrow unittest{
    assert(["hi", "hellow", "world", "hi"].indexOf("hellow")    == 1 );
    assert(["hi", "hellow", "world", "hi"].indexOf("hellow", 1) == 1 );
    assert(["hi", "hellow", "world", "hi"].indexOf("hellow", 2) == size_t.max );

    assert(["hi", "hellow", "world", "hi"].indexOf("hi")    == 0 );
    assert(["hi", "hellow", "world", "hi"].indexOf("hi", 1) == 3 );
    assert(["hi", "hellow", "world", "hi"].indexOf("hi", 2) == 3 );

    assert([5,4,7,11,6].indexOf(57) == size_t.max);
    assert([5,4,7,11,6].indexOf(11) == 3);
}