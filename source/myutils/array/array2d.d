module myutils.array.array2d;

import std.range     : iota;
import std.traits    : Unqual;
import std.algorithm : map;
import std.traits    : isNumeric;
// Container for 2-dimensional arrays stored data in memory continuously
// as in a one-dimensional array

struct Array2d(T)
{
private:
    size_t _height = 0;
    size_t _width  = 0;

    alias data_t = Unqual!T;
    data_t[] _data;

    // i -- line, j -- column
    size_t _index(size_t i, size_t j) const nothrow @nogc pure
    {
        assert(i * _width + j < _data.length);
        return i * _width + j;
    }

    this(size_t height, size_t width, data_t[] init) nothrow pure
    {
        _width  = width;
        _height = height;
        _data   = init;
    }

public:

    // height -- num of lines, width -- num of columns
    this(size_t height, size_t width)
    {
        _width  = width;
        _height = height;
        _data.length = width * height;
    }

    @property size_t height() const { return _height; } // num of lines
    @property size_t width()  const { return _width;  } // num of columns

    @property size_t opDollar(size_t dim : 0)() const { return _height; }
    @property size_t opDollar(size_t dim : 1)() const { return _width;  }

    auto rows() nothrow pure
    {
        return iota(0, _height)
            .map!((size_t i) => _data[_width * i.._width * i + _width]);
    }
    
    // Elements on the diagonal
    auto diag()() const nothrow pure
        if(isNumeric!data_t)
    {
        assert(_height == _width);
        return iota(0, _width)
            .map!((size_t i) => _data[i + i * _width]);
    }
    // Elements on the other diagonal
    auto adiag()() const nothrow pure
        if(isNumeric!data_t)
    {
        assert(_height == _width);
        return iota(0, _width)
            .map!((size_t i) => _data[_width - 1 - i + i * _width]);
    }

    ref data_t opIndexAssign(U)(U value, size_t i, size_t j)
    {
        _data[_index(i, j)] = value;
        return _data[_index(i, j)];
    }

    // Index a single element, e.g., arr[i, j]
    ref data_t opIndex(size_t i, size_t j)
    {
        return _data[_index(i, j)];
    }
    data_t opIndex(size_t i, size_t j) const
    {
        return _data[_index(i, j)];
    }    

    // Index a slice line, e.g., arr[i]
    data_t[] opIndex(size_t i)
    {
        return _data[i * _width .. i * _width + _width];
    }
    data_t[] opIndex(size_t i, size_t[2] jj)
    {
        size_t offset = i * _width;
        return _data[offset + jj[0] .. offset + jj[1]];
    }
    Array2d!T opIndex(size_t[2] ii)
    {
        return Array2d!T(ii[1] - ii[0], _width, _data[_width * ii[0] .. _width * ii[1]]);
    }
    // Support for `x..y` notation in slicing operator for the given dimension.
    size_t[2] opSlice(size_t dim)(size_t start, size_t end)
        if (dim >= 0 && dim < 2)
    {
        assert(start >= 0 && end <= this.opDollar!dim);
        return [start, end];
    }

    void fill(data_t default_value)
    {
        foreach(row; this.rows()) {
            foreach(ref el; row) {
                el = default_value;
            }
        }
    }

    data_t trace()() const
        if(isNumeric!data_t)
    {
        assert(_height == _width);

        data_t sum = data_t(0);
        foreach(size_t i; 0.._width) {
            sum += _data[i + i * _width];
        }
        return sum;
    }
    // The sum of the elements on the other diagonal
    data_t atrace()() const
        if(isNumeric!data_t)
    {
        assert(_height == _width);

        data_t sum = data_t(0);
        foreach(size_t i; 0.._width) {
            sum += _data[_width - 1 - i + i * _width];
        }
        return sum;
    }

    const(data_t[]) data() const
    {
        return _data;
    }
}

debug {
    import std.stdio : write, writeln;

    void print(T)(const(Array2d!T) matrix)
    {
        foreach(i; 0..matrix.height) {
            foreach(j; 0..matrix.width) {
                write("[", matrix[i, j], "]\t");
            }
            writeln();
        }
    }
}


//nothrow 
unittest{

    auto matrix = Array2d!size_t (6, 4);
    size_t q = 0;
    foreach(i; 0..matrix.height) {
        foreach(j; 0..matrix.width) {
            matrix[i, j] = (++q);
        }
    }
    // [1]   [2]   [3]   [4] 
    // [5]   [6]   [7]   [8] 
    // [9]   [10]  [11]  [12]    
    // [13]  [14]  [15]  [16]    
    // [17]  [18]  [19]  [20]    
    // [21]  [22]  [23]  [24] 
    assert(matrix[0, 0]     == 1 );
    assert(matrix[1, 1]     == 6 );
    assert(matrix[0, $-1]   == 4 );
    assert(matrix[$-1, 0]   == 21);
    assert(matrix[$-1, $-1] == 24);

    assert( matrix[0]       == [1, 2, 3, 4]);
    assert( matrix[$-1]     == [21, 22, 23, 24]);
    assert( matrix[0, 1..3] == [2, 3]);

    assert( matrix[1..3].width()  == 4);
    assert( matrix[1..3].height() == 2);

    assert( matrix[1..3][0]  == matrix[1]);
    assert( matrix[2..4][1]  == matrix[3]);

    assert( matrix.rows()[0]     == matrix[0]);
    assert( matrix.rows()[$-1]   == matrix[$-1]);
    assert( matrix.rows().length == matrix.height);

    // Fill by ranges
    q = 0;
    foreach(row; matrix.rows()) {
        foreach(ref el; row) {
            el = (++q)*q;
        }
    }
    // [1]    [4]    [9]    [16]    
    // [25]   [36]   [49]   [64]    
    // [81]   [100]  [121]  [144]   
    // [169]  [196]  [225]  [256]   
    // [289]  [324]  [361]  [400]   
    // [441]  [484]  [529]  [576] 
    assert(matrix[0, 0]     == 1  );
    assert(matrix[1, 1]     == 36 );
    assert(matrix[0, $-1]   == 16 );
    assert(matrix[$-1, 0]   == 441);
    assert(matrix[$-1, $-1] == 576);

    {
        matrix = Array2d!size_t (3, 3);
        foreach(i; 0..matrix.height) {
            foreach(j; 0..matrix.width) {
                matrix[i, j] = (i+1)*(j+1)*(j*j+i+1);
            }
        }
        assert(matrix.trace()  == 76);
        assert(matrix.atrace() == 36);
    }
}