module myutils.array.combinations;

//
// Получает все возможные сочетания 
// см юниттесты
//

import std.algorithm : map;
import std.array     : array;

private
T[][] getCombinationsHelper(T)(T[][] lhs, T[][] rhs) 
{
    if(!rhs.length) {
        return lhs;
    }

    T[][] new_lhs;
    new_lhs.reserve(lhs.length * rhs[0].length);

    foreach(ref x; lhs) {
        foreach(ref y; rhs[0]) {
            new_lhs ~= x ~ y;
        }
    }
    return getCombinationsHelper(new_lhs, rhs[1..$]);
}

T[][] getCombinations(T)(T[][] arr) 
{
    if(!arr.length) {
        return arr;
    } else if(1 == arr.length) {
        return arr[0].map!(x => [x]).array;
    }
    return getCombinationsHelper(arr[0].map!(x => [x]).array, arr[1..$]);
}


nothrow unittest{
    assert((int[][]).init.getCombinations == [] );

    assert([
        [1,2,3],
        [4, 5]
    ].getCombinations == [[1, 4], [1, 5], [2, 4], [2, 5], [3, 4], [3, 5]] );

    assert([
        [1,2,3]
    ].getCombinations == [[1], [2], [3]] );

    assert([
        [1,2,3],
        [4, 5],
        [6,7]
    ].getCombinations == [[1, 4, 6], [1, 4, 7], [1, 5, 6], [1, 5, 7], [2, 4, 6], [2, 4, 7], [2, 5, 6], [2, 5, 7], [3, 4, 6], [3, 4, 7], [3, 5, 6], [3, 5, 7]] );

    assert([
        [1,2,3],
        [],
        [6,7]
    ].getCombinations == [] );

    assert([
        ["1","2","3"],
        ["4", "5", "6","7"],
    ].getCombinations == [["1", "4"], ["1", "5"], ["1", "6"], ["1", "7"], ["2", "4"], ["2", "5"], ["2", "6"], ["2", "7"], ["3", "4"], ["3", "5"], ["3", "6"], ["3", "7"]] );
}

/+
    (int[][]).init.getCombinations.writeln;
    // []

    [
        [1,2,3],
        [4, 5],
    ].getCombinations.writeln;
    // [[1, 4], [1, 5], [2, 4], [2, 5], [3, 4], [3, 5]]

    [
        [1,2,3],
        [4, 5],
        [6,7]
    ].getCombinations.writeln;
    // [[1, 4, 6], [1, 4, 7], [1, 5, 6], [1, 5, 7], [2, 4, 6], [2, 4, 7], [2, 5, 6], [2, 5, 7], [3, 4, 6], [3, 4, 7], [3, 5, 6], [3, 5, 7]]

    [
        [1,2,3],
        [],
        [6,7]
    ].getCombinations.writeln;
    // []

    [
        ["1","2","3"],
        ["4", "5", "6","7"],
    ].getCombinations.writeln;
    // [["1", "4"], ["1", "5"], ["1", "6"], ["1", "7"], ["2", "4"], ["2", "5"], ["2", "6"], ["2", "7"], ["3", "4"], ["3", "5"], ["3", "6"], ["3", "7"]]

+/