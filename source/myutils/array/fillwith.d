module myutils.array.fillwith;

import std.range  : isInputRange, isInfinite, ElementType,  front, popFront, empty;
import std.traits : Unqual;
//import myutils.array;

void fillWith(T, Range)(ref T[] arr, Range range) pure
    if(isInputRange!Range && !isInfinite!Range && is(Unqual!(ElementType!Range) == Unqual!T))
{
    for(size_t i = 0; i < arr.length && !range.empty; ++i, range.popFront()) {
        arr[i] = range.front;
    }
}

nothrow unittest{
    import std.range     : iota, take;
    import std.array     : array;
    import std.algorithm : filter, map;

    {
        auto rng = iota(0, 20).filter!`a%2`;
        int[] arr;
        arr.length = 10;
        arr.fillWith(rng);
        assert(arr == rng.array);
    }
    {
        auto rng = iota(0, 10).filter!`a%2`;
        int[] arr;
        arr.length = 10;
        arr.fillWith(rng);
        assert(arr[0..5] == rng.array);
    }
    {
        auto rng = iota(0, 20).filter!`a%2`;
        int[] arr;
        arr.length = 5;
        arr.fillWith(rng);
        assert(arr == rng.take(5).array);
    }
    {
        auto rng = iota(0, 20).filter!`(a%2)`.map!`a.to!string`;
        string[] arr;
        arr.length = 10;
        arr.fillWith(rng);
        assert(arr == rng.array);
    }
}