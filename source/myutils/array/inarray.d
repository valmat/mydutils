module myutils.array.inarray;

import myutils.array;

import std.stdio;


bool has(T)(auto ref const T[] r, auto ref const T src, size_t start = 0) nothrow pure
{
    return indexOf(r, src, start) < size_t.max;
}

bool has(alias pred, T)(auto ref const T[] r, size_t start = 0)
{
    return indexOf!pred(r, start) < size_t.max;
}

size_t indexOfSorted(T)(auto ref const T[] arr, auto ref const T src) nothrow pure @nogc
{
    if(!arr.length) {return size_t.max;}
    if(src < arr[0] && src > arr[$-1]) {return size_t.max;}

    size_t lb = 0, rb = arr.length - 1;
    while(rb > lb + 1) {
        size_t mb = (lb + rb) / 2;

        if(arr[lb] == src) {
            return lb;
        } else if(arr[rb] == src) {
            return rb;
        } else if(arr[mb] == src) {
            return mb;
        } else if(arr[mb] < src) {
            lb = mb;
        } else {
            rb = mb;
        }
    }

    return size_t.max;
}
bool hasSorted(T)(auto ref const T[] r, auto ref const T src) nothrow pure @nogc
{
    return indexOfSorted(r, src) < size_t.max;
}

nothrow unittest{
    import std.range : iota;
    import std.array : array;
    {
        auto arr = [1,20,31,33,41,42,500,647];
        assert(arr.indexOfSorted(42) == 5);
        assert(arr.indexOfSorted(43) == size_t.max);
    }
    {
        auto arr = ["aa","ab","bc","kl"];
        assert(arr.indexOfSorted("bc")  == 2);
        assert(arr.indexOfSorted("eee") == size_t.max);
    }
    {
        auto arr = iota(0, 100, 2).array;
        assert(arr.indexOfSorted(52) == 26);
        assert(arr.indexOfSorted(53) == size_t.max);
    }
}

//nothrow unittest{
//    assert(["hi", "hellow", "world", "hi"].indexOf("hellow")    == 1 );
//    assert(["hi", "hellow", "world", "hi"].indexOf("hellow", 1) == 1 );
//    assert(["hi", "hellow", "world", "hi"].indexOf("hellow", 2) == size_t.max );

//    assert(["hi", "hellow", "world", "hi"].indexOf("hi")    == 0 );
//    assert(["hi", "hellow", "world", "hi"].indexOf("hi", 1) == 3 );
//    assert(["hi", "hellow", "world", "hi"].indexOf("hi", 2) == 3 );

//    assert([5,4,7,11,6].indexOf(57) == size_t.max);
//    assert([5,4,7,11,6].indexOf(11) == 3);
//}